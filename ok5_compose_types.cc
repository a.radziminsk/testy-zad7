#include "../tri_list.h"

struct A {
    int operator()(int x) {
        return x;
    }
} a;

int main() {
    tri_list<int, double, bool> l;
    auto f = [](int x) { return x; };
    l.modify_only<int>(compose<int>(a, f));
}
